package br.ufc.list6.q1

trait ISet {
  
  def insertSet(e: String): Set
  def remove(e:String): Set
  def complement(): Set 
  def union(c: Set): Set 
  def intersection(c: Set): Set 
  def subtraction(c: Set): Set  
  def contains(c: Set): Boolean 
  def size(): Int 
  def isThere(valor: String): Boolean 
  def same(c: Set): Boolean
  def printSet(): String
}