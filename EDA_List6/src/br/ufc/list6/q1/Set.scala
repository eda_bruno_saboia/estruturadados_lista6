package br.ufc.list6.q1

import com.sun.org.apache.xalan.internal.xsltc.compiler.ForEach


 object Set {
  def createSet(m: Map, e: Array[String]): Set = {
    var s = new Set(m);
    if (e != null) {
      e.foreach { x => s = s.insertSet(x)};
    }
    return s;
  }
};


class Set(m: Map, b: Int = 0) extends ISet {

  private var bits = b;

  override def insertSet(e: String): Set ={ 
    new Set(m, bits | m.get(e))
    }
  
  override def remove(e:String): Set ={
    new Set(m, bits & ~(~bits | m.get(e)))
  }

  override def complement(): Set ={
    new Set(m, ~this.bits)
  } 
  
  override def union(c: Set): Set ={ 
    new Set(m, this.bits | c.bits)
    }

  override def intersection(c: Set): Set ={ 
    new Set(m, this.bits & c.bits)
  }

  override def subtraction(c: Set): Set ={
    new Set(m, this.bits & (~c.bits))
  }
 
  override def contains(c: Set): Boolean ={
    (this.subtraction(c)).bits == 0
  }
 
  override def size(): Int ={
    Integer.bitCount(bits & m.mask)
  }

  override def isThere(valor: String): Boolean ={
    (this.bits & m.get(valor)) != 0
  }

  override def same(c: Set): Boolean ={ 
    ((this.contains(c)) && (c.size() == this.size()))
  }
  
  override def printSet(): String = {

    if (bits == 0) "{}"

    var conj = new Array[String](0);

    for (i <- 0 until 31) {
      var e = m < (1 << i & bits);

      if (e != null) {
        conj = conj :+ e.toString();
      }
    }

    return conj.mkString("{", ", ", "}");

  }
  
  
}
