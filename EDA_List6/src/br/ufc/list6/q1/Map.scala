package br.ufc.list6.q1

import scala.collection.mutable.HashMap

class Map(e: Array[String]) {
  
  private var toMap = new HashMap[String, Int]();
  private var toPrint = new HashMap[Int, String]();
  private var bits = 1;

  if(e != null) {
    e.foreach { x => this << x };
  }
  
  def <<(e: String): Map = {
    toMap.put(e, 1 << toMap.size);
    toPrint.put(1 << toPrint.size, e);

    bits = (bits << 1) | 1;
    this
  }

  def mask: Int = bits;

  def get(e: String): Int ={
    toMap.get(e) match { 
      case Some(x) => x; 
      case None => 0 
      };
  }

  def <(i: Int): String = toPrint.get(i) match { case Some(x) => x; case None => null };
  
  override def toString = toMap.mkString("{", ",\n", "}");

}

 
