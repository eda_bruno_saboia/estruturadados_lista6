package br.ufc.list6.q1

object Main {
  def main(args: Array[String]) {
  
  var m = new Map(Array("a", "b", "c", "d", "e", "f", "g", "h", "i"))

  println("Conjunto A:")
  var a = Set.createSet(m, Array("a", "b"))
  println(a.printSet() + "\n\n")

  println("Inserir no conjunto A: \"c\"")
  a = a.insertSet("c")
  println(a.printSet() + "\n\n")
  
  println("Remover no conjunto A: \"c\"")
  a = a.remove("c")
  println(a.printSet() + "\n\n")
  
  println("Criar conjunto B:")
  var b = Set.createSet(m, Array("d", "e", "f"))
  println(b.printSet() + "\n\n")
  println("Uniao conjunto A e B")
	var c = a.union(b);
  println("Conjunto C = (A U B): "+c.printSet() + "\n\n")

  println("Criar conjunto D:")
  var d = Set.createSet(m, Array("f", "g", "h"))
  println(d.printSet() + "\n\n")
  println("Intersecao conjunto B e D")
  var e = b.intersection(d)
  println(e.printSet() + "\n\n")
  
  println("Diferença entre B e D (B - D):")
  var f = b.subtraction(d)
  println(f.printSet() + "\n\n")
  
  println("Verificar se A é subconjunto de C:")
  println(a.contains(c))
  println
  
  println("Verificar se B e D são iguais:")
  println(b.same(d))
  println
  
  println("Criar conjunto G com os elementos: (a,b)")
  var g = Set.createSet(m, Array("a", "b"))
  println("Verificar se A e G são iguais:")
  println(a.same(g))
  println
  
  println("Gerar complemento de B")
  println(b.complement().printSet())
  println
  
  println("Verificar se o elemento a pertende ao Conjunto A:")
  println(a.isThere("a"))
  println
  
  println("Verificar o tamanho de A (a,b):")
  println(a.size())
  
  }
}